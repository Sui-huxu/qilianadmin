'use strict';
const path = require('path');

function resolve(dir) {
	return path.join(__dirname, dir);
}

module.exports = {
	// publicPath: '/',
	publicPath: './', // 打包使用
	outputDir: 'dist',
	assetsDir: 'static',
	productionSourceMap: false,
	configureWebpack: {
		resolve: {
			alias: {
				'@': resolve('src'),
			},
		},
	},
	chainWebpack: config => {
		// 修复HMR
		config.resolve.symlinks(true);
	},
  lintOnSave: false
};
