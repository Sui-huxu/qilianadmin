import Vue from 'vue';
import VueRouter from 'vue-router';
import Layout from '@/Layout';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		redirect: '/home',
		component: Layout,
		children: [
			{
				path: 'user',
				component: () => import('@/views/user/index'),
				name: 'User',
				meta: { title: '用户' },
			},
			{
				path: 'detail',
				component: () => import('@/views/detail/index'),
				name: 'Detail',
				meta: { title: '详情' },
			},
			{
				path: 'home',
				component: () => import('@/views/home/index'),
				name: 'Home',
				meta: { title: '首页' },
			},
			{
        path: 'courselecture',
        component: () => import('@/views/courselecture/index'),
				name: 'CourseLecture',
				meta: { title: '首页' },
			},
			{
				path: '/bigdata',
				component: () => import('@/views/bigdata/index'),
				redirect: '/bigdata/academy',
				children: [
					{
						path: '/bigdata/academy',
						component: () => import('@/views/bigdata/academy/index'),
						name: 'Academy',
						meta: { title: '院校查询' },
					},
					{
						path: '/bigdata/major',
						component: () => import('@/views/bigdata/major/index'),
						name: 'Major',
						meta: { title: '专业查询' },
					},
					{
						path: '/bigdata/matriculate',
						component: () => import('@/views/bigdata/matriculate/index'),
						name: 'Matriculate',
						meta: { title: '历年录取查询' },
					},
					{
						path: '/bigdata/institutionDetails',
						component: () => import('@/views/bigdata/institutionDetails/index'),
						name: 'InstitutionDetails',
						meta: { title: '院校概况' },
					},
					{
						path: '/bigdata/majorDetails',
						component: () => import('@/views/bigdata/majorDetails/index'),
						name: 'MajorDetails',
						meta: { title: '专业概况' },
					},
				],
			},
			{
				path: 'smart',
				component: () => import('@/views/smart/index'),
				name: 'Smart',
				meta: { title: '智能填选' , isAuth: true},
			},
			{
				path: 'proChaek',
				component: () => import('@/views/proChaek/index'),
				name: 'proChaek',
				meta: { title: '专业查询' },
			},
			{
				path: 'trisome',
				component: () => import('@/views/trisome/index'),
				name: 'Trisome',
				meta: { title: '三位一体' },
			},
			{
				path: 'optional',
				component: () => import('@/views/optional/index'),
				name: 'Optional',
				meta: { title: '选科查询' },
			},
			{
				path: '/shortcut',
				component: () => import('@/views/shortcut/index'),
				redirect: '/shortcut/academy',
				children: [
					{
						path: '/shortcut/segmentsubline',
						component: () => import('@/views/shortcut/segmentsubline/index'),
						name: 'SegmentSubline',
						meta: { title: '段次线查询' },
					},
					{
						path: '/shortcut/seatingarrangement',
						component: () =>
							import('@/views/shortcut/seatingarrangement/index'),
						name: 'SeatingArrangement',
						meta: { title: '位次查询' },
          },
					{
						path: '/shortcut/advanceapproval',
						component: () => import('@/views/shortcut/advanceapproval/index'),
						name: 'AdvanceApproval',
						meta: { title: '提前批查询' },
					},
					{
						path: '/shortcut/zhejing',
						component: () => import('@/views/shortcut/zhejing/index'),
						name: 'zhejing',
						meta: { title: '浙警查询' },
					},
					{
						path: '/shortcut/ranklist',
						component: () => import('@/views/shortcut/ranklist/index'),
						name: 'RankList',
						meta: { title: '院校排行榜' },
					},
				],
			},

			{
				path: 'careerTest',
				component: () => import('@/views/careerTest/index'),
				name: 'CareerTest',
				meta: { title: '职业测试' },
			},
			{
				path: 'careerTest/detailTest',
				component: () => import('@/views/careerTest/detailTest/index'),
				name: 'DetailTest',
				meta: { title: '各项测试' },
			},
			{
				path: 'advanced',
				component: () => import('@/views/advanced/index'),
				name: 'Advanced',
				meta: { title: '高职提前' },
			},
      {
        path: 'advised',
        component: () => import('@/views/advised/index'),
        name: 'Advised',
        meta: { title: '名师咨询' },
      }
		],
  },
  {
						path: '/shortcut/seatingarrangement/wechat',
						component: () =>
							import('@/views/shortcut/seatingarrangement/wechat'),
						name: 'SeatingArrangementWechat',
						meta: { title: '位次查询' },
					},
];





const router = new VueRouter({
	mode: 'hash', // 打包使用
	// mode: 'history',
	base: process.env.BASE_URL,
	routes,
});



export default router;
