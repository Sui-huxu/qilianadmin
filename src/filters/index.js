const subject = [
	{
		label: 0,
		value: '不限',
	},
	{
		label: 1,
		value: '物理',
	},
	{
		label: 2,
		value: '化学',
	},
	{
		label: 3,
		value: '生物',
	},
	{
		label: 4,
		value: '政治',
	},
	{
		label: 5,
		value: '历史',
	},
	{
		label: 6,
		value: '地理',
	},
	{
		label: 7,
		value: '技术',
	},
];

const batchOptions = [
	{
		value: 5,
		label: '普通类',
	},
	{
		value: 2,
		label: '艺术类',
	},
	{
		value: 4,
		label: '体育类',
	},
];
const majorOptions = [
	{
		value: 'B',
		label: '美术类',
	},
	{
		value: 'c',
		label: '音乐类',
	},
	{
		value: 'G',
		label: '播音主持类',
	},
	{
		value: 'H',
		label: '编导类',
	},
	{
		value: 'E',
		label: '影视表演类',
	},
	{
		value: 'J',
		label: '摄制类',
	},
	{
		value: 'D',
		label: '舞蹈类',
	},
	{
		value: 7,
		label: '时装表演类',
	},
];

export const getBatch = (val, symbol = ',') => {
	if (!val) {
		return '';
	}
	const index = batchOptions.findIndex(v => v.value == val);
	if (index == -1) {
		const idx = majorOptions.findIndex(v => v.value == val);
		return majorOptions[idx].label;
	}
	return batchOptions[index].label;
};

export const getSubject = (arr, symbol = ',') => {
	if (!arr.length) {
		return '';
	}

	let list = [];
	subject.map((v, i) => {
		const index = arr.findIndex(item => v.label == item);
		if (index == -1) {
			return;
		}
		list.push(subject[i].value);
	});
	return list.join(symbol);
};

export const getSubjecttoJoin = val => {
	if (!val) {
		return '';
	}
	const arr = val.split('');
	let list = [];
	subject.map((v, i) => {
		const index = arr.findIndex(item => v.label == Number(item));
		if (index == -1) {
			return;
		}
		list.push(subject[i].value);
	});
	return list.join(',');
};
