import request from '@/utils/request';
// 我的志愿单接口
export function aspiration(data) {
	return request({
		url: '/system/api/member/aspiration',
		method: 'put',
		data,
	});
}