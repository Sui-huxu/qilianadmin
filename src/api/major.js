import request from '@/utils/request';

// 根据分数查询参考位次
export function selectPrecedence(params) {
	return request({
		url: '/data/score/selectPrecedence',
		method: 'get',
		params,
	});
}
//   查询所有城市
export function selectCity() {
	return request({
		url: '/data/subject/selectCity',
		method: 'get',
	});
}
//
//查询所有专业
export function selectSubject() {
	return request({
		url: '/data/subject/selectSubject',
		method: 'get',
	});
}
//根据条件查询城市
export function selectUniversities(data) {
	return request({
		url: '/data/subject/selectUniversities',
		method: 'post',
		// headers: { 'Content-Type': 'application/json' },
		data,
	});
}
