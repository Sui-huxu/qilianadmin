import request from '@/utils/request';

//根据id查专业名称
export function selectMajorNameById(id) {
  return request({
    url: `/data/bigData/querySubName`,
    method: 'post',
    data: id
  });
}

//基准年份
export function selectBaseYear() {
  return request({
    url: '/data/bigData/timeBasis',
    method: 'get'
  });
}

// 省份查询
export function selectProvince() {
	return request({
		url: '/data/bigData/selectProvince',
		method: 'get'
	});
}

// 查询院校双一流轮次信息
export function selectCollegeBatchByCode(params) {
	return request({
		url: '/data/bigData/selectCollegeBatchByCode',
		method: 'get',
		params,
	});
}

export function selectCollegesByParams(data) {
	return request({
		url: '/data/bigData/selectCollegesByParams',
		method: 'post',
		data,
	});
}

export function selectMajors(params) {
	return request({
		url: '/data/bigData/selectMajors',
		method: 'get',
		params,
	});
}

export function selectCollegeInfoByCode(id) {
	return request({
		url: `/data/bigData/selectCollegeInfoByCode/${id}`,
		method: 'get',
	});
}

export function selectMajorInfo(id) {
	return request({
		url: `/data/bigData/selectMajorInfo/${id}`,
		method: 'get',
	});
}

// 录取计划
export function selectHistoryAdmissionPlan(id, year) {
	return request({
		url: `/data/bigData/selectHistoryAdmissionPlan?id=${id}&year=${year}`,
		method: 'get'
	});
}

export function selectAdmissionPlan(id, year) {
	return request({
		url: `/data/bigData/selectAdmissionPlan?id=${id}&year=${year}`,
		method: 'get'
	})
}

// 提前录取批次
export function selectEarlyAdmission(id, year) {
	return request({
		url: `/data/bigData/selectEarlyAdmission?id=${id}&year=${year}`,
		method: 'get'
	})
}

// 查询招生简章
export function selectStudentRecruitmentBrochure(id) {
	return request({
		url: `/data/bigData/queryRules/${id}`,
		method: 'post'
	})
}

export function companyLevel(){
  return request({
    url: 'system/api/dict/data/type/company_level',
    method: "get"
  })
}