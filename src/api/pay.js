import request from '@/utils/request';

//查询套餐列表
export function selectPackageList() {
  return request({
    url: '/pay/api/buyer/payment/cart',
    method: 'get',
  });
}

//创建订单
export function createOrder(data) {
  return request({
    url:`/pay/api/buyer/order/create?id=${data.id}&nums=${data.nums}`,
    method: 'post',
    data,
  });
}

//支付订单
export function payOrder(paymentMethod, paymentClient, clientType,sn) {
  return request({
    url: `/pay/api/payment/cashier/pay/${paymentMethod}/${paymentClient}?clientType=${clientType}&sn=${sn}`,
    method: 'get'
  });
}

//查询支付结果
export function pollPayResult(sn) {
  return request({
    url: `/pay/api/payment/cashier/result?sn=${sn}`,
    method: 'get'
  });
}

//查询订单
export function selectOrderList(data) {
  return request({
    url: '/pay/api/buyer/order/list',
    method: 'post',
    data,
  });
}

//取消订单
export function cancelOrder(sn) {
  return request({
    url: `/pay/api/buyer/order/cancel/${sn}`,
    method: 'delete',
    data,
  });
}

//删除订单
export function deleteOrder(sn) {
  return request({
    url: `/pay/api/buyer/order/${sn}`,
    method: 'delete',
    data,
  });
}