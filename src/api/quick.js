import request from "@/utils/request";

// 院校排行榜
export function selectRanking(type) {
  return request({
    url: `/data/quickTools/selectRanking/${type}`,
    method: "get",
  });
}

// 位次查询
export function selectPrecedence(params) {
  return request({
    url: "/data/quickTools/selectPrecedence",
    method: "post",
    params,
  });
}

// 位次线查询
export function selectSegmentLine() {
  return request({
    url: "/data/quickTools/selectSegmentLine/web",
    method: "get",
  });
}

// 警校查询
export function selectPoliceSchool(params) {
  return request({
    url: `/data/quickTools/selectPoliceSchool?pageNo=${params.pageNo}
	&pageSize=${params.pageSize}&year=${params.year}`,
    method: "get",
  });
}

// 提前批查询
export function selectAdvanceBatch(params) {
	return request({
	  url: `/data/quickTools/selectAdvanceBatch?year=${params.year}&type=1&name=${params.name}&pageNo=${params.pageNo}&pageSize=${params.pageSize}`,
	  method: "get",
	});
  }