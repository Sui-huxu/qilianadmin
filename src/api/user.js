import request from "@/utils/request";

//判断是否设置密码
export function isSetPassword() {
  return request({
    url: "/system/api/member/profile/password/init",
    method: "get",
  });
}
//上传头像
export function uploadAvatar(data) {
  return request({
    url: "/system/api/member/profile/picture",
    method: "post",
    data,
  });
}

//通过state登录
export function loginByState(state) {
  return request({
    url: `/auth/oauth/api/wechat/login?status=${state}`,
    method: "get",
  });
}

//获取预约验证码
export function getAppointmentCode(mobile) {
  return request({
    url: `/system/api/planner/captcha?mobile=${mobile}`,
    method: "get",
  });
}

//预约规划师
export function appointment(data) {
  return request({
    url: `/system/api/planner/subscribe/${data.id}`,
    method: "put",
    data,
  });
}

//删除艺术成绩
export function delArtScore() {
  return request({
    url: `/system/api/member/score`,
    method: "delete",
  });
}

export function captcha(params) {
  return request({
    url: "/auth/oauth/api/captcha" + "?mobile=" + params.mobile,
    method: "get",
  });
}

//获取收藏院校
export function collection() {
  return request({
    url: "/system/api/member/collect/compare/univ",
    method: "get",
  });
}

//获取志愿单
export function getAspiration() {
  return request({
    url: `/system/api/member/aspiration`,
    method: "get",
  });
}

//新增志愿单
export function addAspiration(data) {
  return request({
    url: `/system/api/member/aspiration`,
    method: "put",
    data,
  });
}

//根据志愿单方案id获取志愿列表
export function getAspirationList(plan) {
  return request({
    url: `/system/api/member/aspiration/list/plan/${plan}`,
    method: "get",
  });
}

//新增志愿列表
export function addAspirationList(data) {
  return request({
    url: `/system/api/member/aspiration/list`,
    method: "put",
    data,
  });
}

//导出志愿
export function exportAspiration(plan) {
  return request({
    url: `/system/api/member/aspiration/list/export/${plan}`,
    method: "get",
  });
}

export function login(params) {
  return request({
    url: `/auth/oauth/api/token?mobile=${params.mobile}&grant_type=${params.grant_type}&captcha=${params.captcha}&password=${params.password}`,
    method: "post",
  });
}

export function render(params) {
  return request({
    url: `/auth/oauth/api/wechat/render`,
    method: "get",
  });
}

//获取用户信息
export function profile(params) {
  return request({
    url: `/system/api/member/profile`,
    method: "get",
  });
}

//更新用户信息
export function profileUpdate(data) {
  return request({
    url: `/system/api/member/profile?userGknf=${data.userGknf}&userGz=${data.userGz}&userSf=${data.userSf}&userXb=${data.userXb}&userXm=${data.userXm}`,
    method: "post",
  });
}

//院校收藏
export const collect = (query) => {
  return request({
    url: `/system/api/member/collect/univ/${query.id}/${query.likeType}`,
    method: "post",
  });
};

//获取收藏列表
export const compare = () => {
  return request({
    url: `/system/api/member/collect/compare/univ`,
    method: "get",
  });
};
