import request from '@/utils/request';

// 院校层次字典
export function selectCollegeLevel() {
  return request({
    url: '/system/api/dict/data/type/company_level',
    method: 'get'
  });
}

// 年份标准字典
export function selectYear() {
  return request({
    url: '/data/bigData/timeBasis',
    method: 'get'
  });
}

// 综合科目字典
export function selectSubject() {
  return request({
    url: '/system/api/dict/score/category',
    method: 'get'
  });
}