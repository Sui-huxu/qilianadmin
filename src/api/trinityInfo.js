import request from '@/utils/request';

export function selectMajors(data) {
	return request({
		url: '/data/trinityInfo/selectMajors',
		method: 'post',
		data,
	});
}

export function reset(data) {
	return request({
		url: '/system/api/member/profile/password/reset',
		method: 'post',
		data,
	});
}
export function info(data) {
	return request({
		url: '/system/api/member/profile',
		method: 'get',
		data,
	});
}
