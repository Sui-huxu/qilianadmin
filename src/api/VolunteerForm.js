import request from "@/utils/request";


export function delalldata(data) {
  return request({
    url: `/system/api/member/aspiration/list/plan/${data.plan}`,
    method: "DELETE",
    data,
  });
}
export function deldata(data) {
  return request({
    url: `/system/api/member/aspiration/list/${data.id}`,
    method: "DELETE",
    data,
  });
}
export function exportdata(data) {
  return request({
    url: `/system/api/member/aspiration/list/export/${data.plan}`,
    method: "get",
    data,
    responseType: "blob",
  });
}

export function postorder(data) {
  return request({
    url: `/system/api/member/aspiration/save`,
    method: "post",
    data,
  });
}
