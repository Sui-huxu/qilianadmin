import request from '@/utils/request';

export function analysisSubject() {
	return request({
		url: '/data/optionalMajors/analysisSubject',
		method: 'get',
	});
}

export function selectMajors(data) {
	return request({
		url: '/data/optionalMajors/selectMajors',
		method: 'post',
		data,
	});
}
//成绩管理 -- 获取成绩信息
export function getMemberScore() {
	return request({
		url: '/system/api/member/score',
		method: 'get',
	});
}
//成绩管理 -- 添加/更新成绩信息
export function postMemberScore(data) {
	return request({
		url: `/system/api/member/score?examLevel=${data.examLevel}&examScore=${data.examScore}&subject=${data.subject}&extraSubject=${data.extraSubject}&extraSubjectLevel=${data.extraSubjectLevel}&extraSubjectScore=${data.extraSubjectScore}`,
		method: 'post',
	});
}

//字典查询

//选考科目
export function getSubject() {
	return request({
		url: '/system/api/dict/score/subject',
		method: 'get',
	});
}
//综合科目 
export function getCategory() {
	return request({
		url: '/system/api/dict/score/category',
		method: 'get',
	});
}