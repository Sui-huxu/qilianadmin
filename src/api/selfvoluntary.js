import request from '@/utils/request';
// 我的志愿接口
export function aspirationdata(data) {
	return request({
		url: '/system/api/member/aspiration',
		method: 'get',
		data,
	});
}



export function addAspiration(data) {
	return request({
		url: `/system/api/member/aspiration`,
		method: 'post',
		data
	});
}

export function listdata(data) {
	return request({
		url: `/system/api/member/aspiration/list/plan/${data.plan}`,
		method: 'get',
	});
}

export function putListdata(data) {
	return request({
		url: `/system/api/member/aspiration/list/plan/${data.plan}`,
		method: 'put',
	});
}

export function deleteinfo(data) {
	return request({
		url: `/system/api/member/aspiration/${data.id}`,
		method: 'delete',
	});
}