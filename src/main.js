import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import 'normalize.css/normalize.css';
// import '/src/assets/css/iconfont.css';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '@/styles/index.scss';
import '@/styles/element-variables.scss';
import '@/styles/iconfonts/iconfonts.scss';

import  VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(VueQuillEditor)

import * as filters from './filters';
import axios from 'axios'



Vue.config.productionTip = false;
Vue.prototype.$axios = axios
Vue.use(ElementUI);
Vue.prototype.baseImgUrl = "https://qlsx.oss-cn-hangzhou.aliyuncs.com/university/"

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key]);
});

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
