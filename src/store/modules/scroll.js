const state = {
	isScroll: false,
	userState:0
};

const mutations = {
	SET_SCROLL: (state, isScroll) => {
		state.isScroll = isScroll;
	},
	SET_USERSTATE:(state, userState) => {
		state.userState = userState
	}
};

const actions = {
  setScroll ({ commit }, isScroll) {
		commit('SET_SCROLL', isScroll);
	},
};

export default {
	namespaced: true,
	state,
	mutations,
	actions,
};
