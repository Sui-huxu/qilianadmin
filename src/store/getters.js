const getters = {
  isScroll: state => state.scroll.isScroll,
  userState: state => state.scroll.userState,
}
export default getters
